package security;

import com.dropbox.core.DbxRequestConfig;

public class UserSession {
	private DbxRequestConfig requestConfig;
	
	public UserSession(DbxRequestConfig requestConfig) {
		this.setRequestConfig(requestConfig);
	}

	public DbxRequestConfig getRequestConfig() {
		return requestConfig;
	}

	private void setRequestConfig(DbxRequestConfig requestConfig) {
		this.requestConfig = requestConfig;
	}	
}
