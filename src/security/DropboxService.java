package security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;

/**
 * Responsável por toda a conexão à API da Dropbox
 * @author root
 *
 */
public abstract class DropboxService {
		
	/**
	 * Verifica se já existe autorização.
	 * @return true se já existir autorização, false caso contrário
	 * @throws IOException
	 */
	public static boolean getToken(String accessToken) throws IOException {
		if((accessToken = Access.getAccessToken()) != null)
			return true;
		else
			return false;
	}
	
	/**
	 * Verifica se o código introduzido dá autorização de acesso.
	 * @param code
	 * @return true se deu autorização, false caso contrário.
	 * @throws DbxException
	 */
	public static boolean getToken(String code, DbxWebAuthNoRedirect webAuth, String accessToken) throws DbxException {
		// This will fail if the user enters an invalid authorization code.
		try {		
			DbxAuthFinish authFinish = webAuth.finish(code);	//When you successfully complete the authorization process, the Dropbox server returns this information to you.	
	        accessToken = authFinish.accessToken;				//An access token that can be used to make Dropbox API calls.
	        return true;
		}
		catch (Exception err) {
			System.out.println("Erro ao adquirir token a partir do código dado: " + err.getMessage());
			return false;
		}
	}
	
	/**
	 * Use this class to make remote calls to the Dropbox API. 
	 * @param requestConfig
	 * @param accessToken
	 * @return 
	 */
	public static DbxClient getClient(DbxRequestConfig requestConfig, String accessToken) {		
		return new DbxClient(requestConfig, accessToken); 		
	}
	
	/**
	 * Realiza a conexão com a conta do Dropbox do cliente.
	 * @param appKey
	 * @param appSecret
	 * @param clientIdentifier
	 * @param userLocale
	 * @throws IOException
	 * @throws DbxException
	 */
	public static UserSession connect(String appKey, String appSecret, String clientIdentifier, String userLocale, int userID) throws IOException, DbxException {
		DbxAppInfo appInfo;
		DbxRequestConfig requestConfig;
		DbxWebAuthNoRedirect webAuth;
		String accessToken = new String();
		
		if(getToken(accessToken)) {
	        requestConfig = new DbxRequestConfig(clientIdentifier, userLocale);
			System.out.println("Conectado com sucesso");
		}
		else {
			appInfo = new DbxAppInfo(appKey, appSecret);
			requestConfig = new DbxRequestConfig(clientIdentifier, userLocale);
			webAuth = new DbxWebAuthNoRedirect(requestConfig, appInfo);
			String authorizeUrl = webAuth.start();
			System.out.println("Go to: " + authorizeUrl
					+ "\n and paste the authorization code here:");
			String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();
			
			if(getToken(code, webAuth, accessToken)) {
				Access.setAccessToken(accessToken);
				System.out.println("Conectado com sucesso");
			}
			else
				System.out.println("Ocorreu um erro");
		}
		return new UserSession(requestConfig);
	}
}
