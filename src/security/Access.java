package security;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;


public class Access {
	public static String getAccessToken() throws IOException {
		final String inputFile = "access.info";
		String str = null;
		
		try (FileInputStream inputStream = new FileInputStream(inputFile)){			
			str = IOUtils.toString(inputStream, "UTF-8");
		}
		catch (Exception err){
			System.out.println("Erro ao adquirir token do ficheiro access.info: " + err.getMessage());
		}
		return str;
	}
	
	public static void setAccessToken(String accessToken) throws IOException {
		final String outputFile = "access.info";
		
		try	(FileOutputStream outputStream = new FileOutputStream(outputFile)){			
			outputStream.write(accessToken.getBytes());
		}
	}
}
